<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>de.fraunhofer.idmt.mico</groupId>
	<artifactId>registration-service</artifactId>
	<version>3.1.1-SNAPSHOT</version>
	<packaging>war</packaging>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.3.2.RELEASE</version>
	</parent>

	<properties>
		<java.version>1.7</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<start-class>de.fraunhofer.idmt.mico.registration.service.Application</start-class>
		<maven.build.timestamp.format>yyMMdd_HHmm</maven.build.timestamp.format>
		<build>${maven.build.timestamp}</build>
	</properties>

	<dependencies>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
		</dependency>

		<!-- MySQL driver -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>

		<!-- PostgreSQL driver -->
		<dependency>
			<groupId>postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>9.1-901-1.jdbc4</version>
		</dependency>


		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<!-- needed for war packaging -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>de.fraunhofer.idmt.mico</groupId>
			<artifactId>registration-service-beans</artifactId>
			<version>${project.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
		</dependency>

		<dependency>
			<groupId>org.jsondoc</groupId>
			<artifactId>spring-boot-starter-jsondoc</artifactId>
			<version>1.2.10</version>
		</dependency>

		<dependency>
			<groupId>org.jsondoc</groupId>
			<artifactId>jsondoc-ui-webjar</artifactId>
			<version>1.2.10</version>
		</dependency>
	</dependencies>

	<scm>
		<connection>scm:git:git@bitbucket.org:mico-project/extractor-registration.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:mico-project/extractor-registration.git</developerConnection>
		<url>git@bitbucket.org:mico-project/extractor-registration.git</url>
		<tag>HEAD</tag>
	</scm>

	<!-- The Spring Boot Maven plugin provides many convenient features: It 
		collects all the jars on the classpath and builds a single, runnable "über-jar", 
		which makes it more convenient to execute and transport your service. It 
		searches for the public static void main() method to flag as a runnable class. 
		It provides a built-in dependency resolver that sets the version number to 
		match Spring Boot dependencies. You can override any version you wish, but 
		it will default to Boot’s chosen set of versions. -->
	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<useReleaseProfile>false</useReleaseProfile>
					<goals>deploy</goals>

					<!-- do not fetch over the Internet the whole repository -->
					<localCheckout>true</localCheckout>

					<!-- the name of the tag of a release in GIT -->
					<tagNameFormat>@{version}</tagNameFormat>

					<!-- make sure all dependencies are available -->
					<preparationGoals>clean install</preparationGoals>

					<pushChanges>false</pushChanges>
				</configuration>

			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>spring-releases</id>
			<url>https://repo.spring.io/libs-release</url>
		</repository>
	</repositories>


	<pluginRepositories>
		<pluginRepository>
			<id>spring-releases</id>
			<url>https://repo.spring.io/libs-release</url>
		</pluginRepository>
	</pluginRepositories>


	<distributionManagement>
		<repository>
			<id>mico</id>
			<url>http://mvn.mico-project.eu/content/repositories/releases/</url>
		</repository>
		<snapshotRepository>
			<id>mico</id>
			<url>http://mvn.mico-project.eu/content/repositories/snapshots/</url>
		</snapshotRepository>
	</distributionManagement>

	<profiles>
		<profile>
			<id>jsondoc-offline</id>
			<properties>
				<jsondoc.path>jsondoc-ui/jsondoc.json</jsondoc.path>
			</properties>
			<dependencies>
				<dependency>
					<groupId>org.jsondoc</groupId>
					<artifactId>jsondoc-ui</artifactId>
					<version>1.2.11</version>
				</dependency>
			</dependencies>
			<build>
				<plugins>
					<plugin>
						<groupId>org.jsondoc</groupId>
						<artifactId>jsondoc-maven-plugin</artifactId>
						<version>1.2.11</version>
						<executions>
							<execution>
								<id>generate-jsondoc</id>
								<phase>package</phase>
								<goals>
									<goal>generate</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<version>${project.version}</version>
							<basePath>http://mico-platform:8030/registration-service</basePath>
							<packages>
								<package>de.fraunhofer.idmt.mico.registration</package>
							</packages>
							<outputFile>${project.build.directory}/jsondoc-ui/jsondoc.json</outputFile>
							<!-- this service uses spring 4 annotations, so we need this scanner, 
								instead of default -->
							<scanner>org.jsondoc.springmvc.scanner.Spring4JSONDocScanner</scanner>
							<playgroundEnabled>false</playgroundEnabled> <!-- optional -->
							<displayMethodAs>URI</displayMethodAs> <!-- optional -->
						</configuration>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-dependency-plugin</artifactId>
						<version>2.10</version>
						<executions>
							<execution>
								<id>unpack</id>
								<phase>process-resources</phase>
								<goals>
									<goal>unpack</goal>
								</goals>
								<configuration>
									<artifactItems>
										<artifactItem>
											<groupId>org.jsondoc</groupId>
											<artifactId>jsondoc-ui</artifactId>
											<type>jar</type>
											<overWrite>true</overWrite>
											<outputDirectory>${project.build.directory}/jsondoc-ui</outputDirectory>
										</artifactItem>
									</artifactItems>
								</configuration>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>com.google.code.maven-replacer-plugin</groupId>
						<artifactId>replacer</artifactId>
						<version>1.5.3</version>
						<executions>
							<execution>
								<id>replace-jsondoc-placeholder</id>
								<phase>package</phase>
								<goals>
									<goal>replace</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<includes>
								<include>${project.build.directory}/jsondoc-ui/jsondoc-ui.html</include>
								<include>${project.build.directory}/site/jsondoc-ui.html</include>
							</includes>
							<replacements>
								<replacement>
									<token>'_JSONDOC_OFFLINE_PLACEHOLDER_'</token> <!-- Do not change this value! -->
									<valueFile>${project.build.directory}/jsondoc-ui/jsondoc.json</valueFile>
								</replacement>
								<replacement>
									<token>class="navbar-brand" href="#"&gt;JSONDoc&lt;/a&gt;</token>
									<value>class="navbar-brand" href="index.html"&gt;Back&lt;/a&gt;</value>
								</replacement>
							</replacements>
						</configuration>
					</plugin>

					<plugin>
						<artifactId>maven-resources-plugin</artifactId>
						<version>2.7</version>
						<executions>
							<execution>
								<id>copy-jsondoc-ui</id>
								<!-- here the phase you need -->
								<phase>site</phase>
								<goals>
									<goal>copy-resources</goal>
								</goals>
								<configuration>
									<outputDirectory>${project.build.directory}/site</outputDirectory>
									<resources>
										<resource>
											<directory>${project.build.directory}/jsondoc-ui/</directory>
											<excludes>
												<exclude>**/META-INF/**</exclude>
												<exclude>jsondoc.json</exclude>
											</excludes>
											<filtering>false</filtering>
										</resource>
									</resources>
								</configuration>
							</execution>
						</executions>
					</plugin>

					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>findbugs-maven-plugin</artifactId>
						<version>3.0.3</version>
					</plugin>
				</plugins>
			</build>

			<reporting>
				<plugins>
					<!-- enable maven-jxr-plugin to link findbugs error with source code. 
						!!! WARNING: This will publish source on generated site !!! -->
					<!-- <plugin> -->
					<!-- <groupId>org.apache.maven.plugins</groupId> -->
					<!-- <artifactId>maven-jxr-plugin</artifactId> -->
					<!-- <version>2.5</version> -->
					<!-- </plugin> -->
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>findbugs-maven-plugin</artifactId>
						<version>3.0.1</version>
						<configuration>
							<effort>Max</effort>
							<xmlOutput>true</xmlOutput>
						</configuration>
					</plugin>

					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-checkstyle-plugin</artifactId>
						<version>2.17</version>
						<reportSets>
							<reportSet>
								<reports>
									<report>checkstyle</report>
								</reports>
							</reportSet>
						</reportSets>
					</plugin>

					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-project-info-reports-plugin</artifactId>
						<version>2.8.1</version>
						<reportSets>
							<reportSet>
								<reports>
									<report>index</report>
									<!-- <report>summary</report> -->
									<!-- <report>project-team</report> -->
									<!-- <report>dependencies</report> -->
									<!-- <report>mailing-list</report> -->
									<!-- <report>cim</report> -->
									<!-- <report>issue-tracking</report> -->
									<!-- <report>license</report> -->
									<!-- <report>scm</report> -->
								</reports>
							</reportSet>
						</reportSets>
					</plugin>
				</plugins>
			</reporting>
		</profile>
		<profile>
		<id>debian</id>
			<build>
				<plugins>
					<plugin>
						<artifactId>jdeb</artifactId>
						<groupId>org.vafer</groupId>
						<executions>
							<execution>
								<phase>package</phase>
								<goals>
									<goal>jdeb</goal>
								</goals>
								<configuration>
									<name>mico-registration-service</name>
									<deb>${project.build.directory}/[[name]]_[[version]]_all.deb</deb>
									<changesOut>${project.build.directory}/[[name]]_[[version]]_all.changes</changesOut>

									<controlDir>${basedir}/src/deb/debian</controlDir>
									<dataSet>

										<data>
											<src>${project.build.directory}/${project.build.finalName}.war</src>
											<dst>mico-registration-service.war</dst>
											<type>file</type>
											<mapper>
												<type>perm</type>
												<prefix>/usr/share/mico</prefix>
												<user>tomcat7</user>
												<group>tomcat7</group>
											</mapper>
										</data>
										<data>
											<src>src/deb/resources/mico-registration-service.xml.tmpl</src>
											<type>file</type>
											<mapper>
												<type>perm</type>
												<prefix>/etc/tomcat7/Catalina/localhost</prefix>
												<user>root</user>
												<group>root</group>
											</mapper>
											<conffile>false</conffile>
										</data>
									</dataSet>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>

