package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fraunhofer.idmt.mico.registration.beans.AddReply;
import de.fraunhofer.idmt.mico.registration.beans.DeleteReply;
import de.fraunhofer.idmt.mico.registration.beans.DeploymentInfo;
import de.fraunhofer.idmt.mico.registration.beans.DeploymentInfoList;
import de.fraunhofer.idmt.mico.registration.beans.FindReply;
import de.fraunhofer.idmt.mico.registration.beans.InfoReply;
import de.fraunhofer.idmt.mico.registration.beans.QueryParameters;
import de.fraunhofer.idmt.mico.registration.beans.UpdateReply;
import de.fraunhofer.idmt.mico.registration.client.RestClient;
import de.fraunhofer.idmt.mico.registration.gen.ExtractorSpecification;
import de.fraunhofer.idmt.mico.registration.service.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest({ "server.port=8030" })
public class ClientIntegrationTests {

	private static final Logger log = LoggerFactory.getLogger(ClientIntegrationTests.class);

	RestTemplate restTemplate = new TestRestTemplate();
	ObjectMapper objectMapper = new ObjectMapper();

	@Test
	public void testHelperMethods() {

		RestClient client = new RestClient("http://127.0.0.1:8030");
		try {
			InfoReply infoReply = client.getServiceInfo();
			assertTrue(infoReply.getMessage().contains("Rock & Roll"));

			log.info("Number of extractor specs: " + infoReply.getNumberOfExtractors());

			byte[] pubKeyBytes = client.getPublicKey();
			assertTrue(pubKeyBytes == null);

		} catch (Exception e) {
			fail(e.toString());
		}
	}

	@Test
	public void testExtractorMethods() {

		String extractorId = null;
		RestClient client = new RestClient("http://127.0.0.1:8030");
		try {
			// ADD
			AddReply addReply = client.addExtractorSpec("src/test/resources/micoRegistration.xml");
			assertTrue(addReply.getMessage().equals("OK"));
			extractorId = addReply.getId();

			// CHECK INFO
			InfoReply infoReply = client.getServiceInfo();
			assertTrue(infoReply.getNumberOfExtractors() == 1);

			// ADD AGAIN - try adding the same spec again
			addReply = client.addExtractorSpec("src/test/resources/micoRegistration.xml");
			assertTrue(addReply.getMessage().startsWith("ERROR"));

			// try adding an invalid XML file
			addReply = client.addExtractorSpec("src/test/resources/micoRegistration-invalid.xml");
			assertTrue(addReply.getMessage().contains("SAXParseException"));

			// UPDATE (full, i.e. replacement)
			client.updateExtractorSpec(extractorId, "src/test/resources/micoRegistration-update.xml");

			// ADD DEPLOYMENT INFO
			DeploymentInfo deploymentInfo = new DeploymentInfo();
			deploymentInfo.setIp("localhost");
			deploymentInfo.setPort(8080);
			deploymentInfo.setExtractorId(extractorId);
			AddReply addDeployment = client.addDeploymentInfo(deploymentInfo, extractorId);
			assertTrue(addDeployment.getMessage().equals("OK"));

			// GET
			ExtractorSpecification spec = client.getExtractorSpec(extractorId);
			assertTrue(spec.getId().equals(extractorId));

			// check if the changes are there
			assertTrue(spec.getMode().get(0).getId().equals("dummy mode"));

			DeploymentInfo oldInfo = null;
			try {
				oldInfo = objectMapper.readValue(spec.getDeployedAt().get(0), DeploymentInfo.class);
				assertTrue(oldInfo != null);
				assertTrue(oldInfo.getPort() == 8080);
			} catch (Exception e) {
				fail(e.toString());
			}

			// UPDATE DEPLOYMENT INFO
			deploymentInfo = new DeploymentInfo();
			deploymentInfo.setId(oldInfo.getId());
			deploymentInfo.setIp("localhost");
			deploymentInfo.setPort(8088);
			deploymentInfo.setExtractorId(extractorId);
			UpdateReply updateDeployment = client.updateDeploymentInfo(deploymentInfo, extractorId);
			assertTrue(updateDeployment.getMessage().equals("OK"));

			// GET
			spec = client.getExtractorSpec(extractorId);
			assertTrue(spec.getId().equals(extractorId));

			log.info("INPUT MIME TYPES: "
					+ spec.getMode().get(0).getInput().get(0).getDataType().getMimeType().toString());

			// check if the changes are there
			assertTrue(spec.getDeployedAt().get(0).toString().contains("8088"));

			// GET DEPLOYMENT INFO
			DeploymentInfoList list = client.getDeploymentInfo(extractorId);
			assertTrue(list.getDeployments().size() == 1);
			assertTrue(list.getDeployments().get(0).getExtractorId().equals(extractorId));
			String deploymentId = list.getDeployments().get(0).getId();

			// GET non-existing spec
			spec = client.getExtractorSpec("fubar");
			assertTrue(spec == null);

			// FIND
			FindReply findReply = client.findExtractorSpecsByName("dummy");
			assertTrue(findReply.getSpecs().size() == 1);

			findReply = client.findExtractorSpecsByModeDescription("fubar");
			assertTrue(findReply.getSpecs().size() == 1);

			// GET ALL
			findReply = client.findExtractorSpecs();
			assertTrue(findReply.getSpecs().size() == 1);

			QueryParameters query = new QueryParameters();
			query.setMode("dummY mod");
			query.setMimeType("MIME");
			query.setSyntacticType("tempuri");
			query.setSemanticType("amE");

			// FIND BY INPUT PARAMS
			findReply = client.findExtractorSpecsByInputParams(query);
			assertTrue(findReply.getSpecs().size() == 1);

			// FIND BY OUTPUT PARAMS
			findReply = client.findExtractorSpecsByInputParams(query);
			assertTrue(findReply.getSpecs().size() == 1);

			// DELETE DEPLOYMENT
			DeleteReply deleteReply = client.deleteDeploymentInfo(deploymentId, extractorId);
			assertTrue(deleteReply.getCount() == 1);

			// DELETE
			deleteReply = client.deleteExtractorSpec(extractorId);
			assertTrue(deleteReply.getCount() == 1 && deleteReply.getMessage().equals("OK"));

			// GET DEPLOYMENTS
			list = client.getDeploymentInfo(extractorId);
			assertTrue(list == null);

		} catch (Exception e) {
			fail(e.toString());
		} finally {
			if (extractorId != null) {
				try {
					client.deleteExtractorSpec(extractorId);
				} catch (Exception exc) {
				}
			}
		}
	}

	@Test
	public void testSchemaUpdate() {
		RestClient client = new RestClient("http://127.0.0.1:8030");
		AddReply addReply;
		try {
			addReply = client.addExtractorSpec("src/test/resources/registration.xml");
			log.info(addReply.toString());
			client.deleteExtractorSpec("mico-extractor-id");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}