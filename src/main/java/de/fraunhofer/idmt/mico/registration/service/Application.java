package de.fraunhofer.idmt.mico.registration.service;

import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


//@SpringBootApplication is a convenience annotation that adds all of the
// following:
// @Configuration tags the class as a source of bean definitions for the
// application context.
// @EnableAutoConfiguration tells Spring Boot to start adding beans based on
// classpath settings, other beans, and various property settings.
// Normally you would add @EnableWebMvc for a Spring MVC app, but Spring
// Boot adds it automatically when it sees spring-webmvc on the classpath.
// This flags the application as a web application and activates key
// behaviors such as setting up a DispatcherServlet.
// @ComponentScan tells Spring to look for other components, configurations,
// and services in the hello package, allowing it to find the
// GreetingController.

@SpringBootApplication
@EnableJSONDoc
@EnableJpaRepositories(basePackages = {"de.fraunhofer.idmt.mico.registration.gen", "de.fraunhofer.idmt.mico.registration.service"})
@EntityScan(basePackages = {"de.fraunhofer.idmt.mico.registration.gen",  "de.fraunhofer.idmt.mico.registration.service"})
public class Application extends SpringBootServletInitializer {
	
	// required for standalone mode
	public static void main(String[] args) {
		// SpringApplication.run(Application.class, args);
		SpringApplication.run(Application.class);
	}

	// required to deploy the war into an application container
	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}
}
