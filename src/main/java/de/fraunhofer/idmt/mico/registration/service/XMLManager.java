package de.fraunhofer.idmt.mico.registration.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.xml.sax.SAXException;

import de.fraunhofer.idmt.mico.registration.gen.ExtractorSpecification;

public class XMLManager {

	@Value("classpath:micoRegistration.xsd")
	private Resource resource;

	private JAXBContext jaxbContext;

	public XMLManager() throws JAXBException {
		jaxbContext = JAXBContext.newInstance(ExtractorSpecification.class);
	}

	public ExtractorSpecification deserialize(InputStream is, boolean validate)
			throws SAXException, IOException, JAXBException {

		// de-serialize with JAXB
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		if (validate) {
			// validation settings
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new StreamSource(this.resource.getInputStream()));
			unmarshaller.setSchema(schema);
		}

		ExtractorSpecification specification = (ExtractorSpecification) unmarshaller.unmarshal(is);

		return specification;

	}

	public String serialize(ExtractorSpecification spec, boolean prettyPrint) throws JAXBException {
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		if (prettyPrint) {
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		}
		StringWriter writer = new StringWriter();
		jaxbMarshaller.marshal(spec, writer);
		return writer.toString();
	}
}
