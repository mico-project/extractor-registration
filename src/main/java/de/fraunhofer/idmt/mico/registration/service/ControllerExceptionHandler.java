package de.fraunhofer.idmt.mico.registration.service;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.xml.sax.SAXException;

import de.fraunhofer.idmt.mico.registration.beans.ErrorMessage;

@ControllerAdvice
public class ControllerExceptionHandler {

	private static final Logger log = Logger
			.getLogger(ControllerExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorMessage handleAllExceptions(Exception e) {
		log.error(e.toString());
		return new ErrorMessage(e.toString());
	}

	@ExceptionHandler({ SAXException.class, JAXBException.class })
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	public ErrorMessage handleJAXBExceptions(Exception e) {
		log.error(e.toString());
		return new ErrorMessage(e.toString());
	}

	@ExceptionHandler({ FileNotFoundException.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorMessage handleFileNotFoundExceptions(Exception e) {
		log.error(e.toString());
		return new ErrorMessage(e.toString());
	}
}