package de.fraunhofer.idmt.mico.registration.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.fraunhofer.idmt.mico.registration.beans.AddReply;
import de.fraunhofer.idmt.mico.registration.beans.DeleteReply;
import de.fraunhofer.idmt.mico.registration.beans.DeploymentInfoList;

import de.fraunhofer.idmt.mico.registration.beans.FindReply;
import de.fraunhofer.idmt.mico.registration.beans.InfoReply;
import de.fraunhofer.idmt.mico.registration.beans.QueryParameters;
import de.fraunhofer.idmt.mico.registration.beans.UpdateReply;
import de.fraunhofer.idmt.mico.registration.gen.ExtractorSpecification;
import de.fraunhofer.idmt.mico.registration.beans.DeploymentInfo;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiVersion;
import org.jsondoc.core.pojo.ApiStage;
import org.jsondoc.core.pojo.ApiVisibility;

@Api(name = "Extractor Registration Service", description = "Methods for managing extractors", group = "MICO Broker", visibility = ApiVisibility.PUBLIC, stage = ApiStage.RC)
@ApiVersion(since = "0.8.0")
@RestController
public class MainController {

	private static final Logger log = Logger.getLogger(MainController.class);

	@Value("${info.app.version}")
	private String version;

	@Value("${info.app.build}")
	private String build;

	@Autowired
	private XMLManager xmlManager;

	@Autowired
	ExctractorSpecRepository specRepo;

	@Autowired
	ObjectMapper objectMapper;

	@Value("classpath:platform.pub")
	private Resource pubKeyResource;

	@ApiMethod(description = "Provides information about the service")
	@RequestMapping(value = "/info", method = RequestMethod.GET, produces = "application/json")
	public InfoReply getInfo() {

		return new InfoReply(this.version + " (" + this.build + ")", "Rock & Roll! :P", this.specRepo.count());
	}

	@ApiMethod(description = "Returns the public key of the platform")
	@RequestMapping(value = "/get/pubkey", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFile() {
		try {
			return new FileSystemResource(this.pubKeyResource.getFile());
		} catch (Exception e) {
			log.error(e.toString());
			return null;
		}
	}

	@ApiMethod(description = "Register a new extractor (via XML file upload - see schema micoRegistration.xsd)")
	@RequestMapping(value = "/add/extractor", method = RequestMethod.POST, produces = "application/json")
	public AddReply add(@RequestParam("file") MultipartFile file) throws Exception {

		// de-serialize and validate
		ExtractorSpecification specification = this.xmlManager.deserialize(file.getInputStream(), true);

		// persist
		try {
			ExtractorSpecification saved = this.specRepo.save(specification);
			log.info("Extractor specification stored: [" + saved.getHjid() + "/" + saved.getId() + "]");
			return new AddReply("OK", saved.getId());
		} catch (Exception e) {
			return new AddReply("ERROR: " + e.getMessage(), null);
		}
	}

	// Spring considers that anything behind the last dot is a file extension
	// such as .json or .xml and trucate it to retrieve your parameter.
	// In order to avoid that, the parameters needs to be specified like
	// {parameter:.+}.
	@ApiMethod(description = "Delete an extractor (and related deployments)")
	@RequestMapping(value = "/delete/extractor/{id:.+}", method = RequestMethod.DELETE, produces = "application/json")
	public DeleteReply delete(@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id)
			throws Exception {

		ExtractorSpecification specification = this.specRepo.findById(id);

		if (specification != null) {
			this.specRepo.delete(specification);
			log.info("Extractor specification deleted: " + id);
			return new DeleteReply("OK", 1);
		} else {
			log.error("Extractor specification not found: " + id);
			return new DeleteReply("ERROR", 0);
		}
	}

	@ApiMethod(description = "Provides information about a specific extractor (as XML)")
	@RequestMapping(value = "/get/extractor/{id}/xml", method = RequestMethod.GET, produces = "application/xml")
	public ExtractorSpecification getSpecificationAsXML(
			@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id) throws Exception {

		return this.specRepo.findById(id);
	}

	@ApiMethod(description = "Provides information about a specific extractor (as JSON)")
	@RequestMapping(value = "/get/extractor/{id}/json", method = RequestMethod.GET, produces = "application/json")
	public ExtractorSpecification getSpecificationAsJSON(
			@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id) throws Exception {

		return this.specRepo.findById(id);
	}

	@ApiMethod(description = "Replacing an existing extractor spec - i.e. the whole document (via XML file upload)")
	@RequestMapping(value = "/update/extractor/{id:.+}", method = RequestMethod.POST)
	public void update(@RequestParam("file") MultipartFile file,
			@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id) throws Exception {

		// delete existing one
		ExtractorSpecification saved = this.specRepo.findById(id);
		if (saved != null) {
			this.specRepo.delete(saved);

			// de-serialize & validate
			ExtractorSpecification specification = this.xmlManager.deserialize(file.getInputStream(), true);
			this.specRepo.save(specification);
		} else {
			throw new Exception("Extractor specification not found: " + id);
		}
	}

	@ApiMethod(description = "Add deployment information for a specific extractor")
	@RequestMapping(value = "/add/deployment/{id:.+}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public AddReply addDeployment(@RequestBody DeploymentInfo deploymentInfo,
			@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id) throws Exception {

		ExtractorSpecification saved = this.specRepo.findById(id);
		if (saved != null) {

			// set unique id for later reference
			deploymentInfo.setId(UUID.randomUUID().toString());

			saved.getDeployedAt().add(this.objectMapper.writeValueAsString(deploymentInfo));
			this.specRepo.save(saved);
			return new AddReply("OK", id);
		} else {
			return new AddReply("ERROR: Extractor specification not found: " + id, null);
		}
	}

	@ApiMethod(description = "Update deployment information for a specific extractor")
	@RequestMapping(value = "/update/deployment/{id:.+}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public UpdateReply updateDeployment(@RequestBody DeploymentInfo deploymentInfo,
			@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id) throws Exception {

		ExtractorSpecification saved = this.specRepo.findById(id);
		if (saved != null) {

			int index = -1;
			for (int i = 0; i < saved.getDeployedAt().size(); i += 1) {
				if (saved.getDeployedAt().get(i).contains(deploymentInfo.getId())) {
					index = i;
				}
			}

			if (index > -1) {
				saved.getDeployedAt().remove(index);
				saved.getDeployedAt().add(this.objectMapper.writeValueAsString(deploymentInfo));
				this.specRepo.save(saved);
				return new UpdateReply("OK", 1);
			} else {
				return new UpdateReply("ERROR: Deployment not found: " + deploymentInfo.getId(), 0);
			}

		} else {
			return new UpdateReply("ERROR: Extractor specification not found: " + id, 0);
		}
	}

	@ApiMethod(description = "Get deployment information for an extractor")
	@RequestMapping(value = "/get/deployments/extractor/{id:.+}", method = RequestMethod.GET, produces = "application/json")
	public DeploymentInfoList getDeploymentInfo(
			@ApiPathParam(description = "The id of the extractor") @PathVariable("id") String id) throws Exception {

		ExtractorSpecification saved = this.specRepo.findById(id);
		if (saved != null) {
			DeploymentInfoList resultList = new DeploymentInfoList();
			resultList.setDeployments(new ArrayList<DeploymentInfo>());

			for (String info : saved.getDeployedAt()) {
				resultList.getDeployments().add(this.objectMapper.readValue(info, DeploymentInfo.class));
			}
			return resultList;

		} else {
			log.error("Extractor specification not found: " + id);
			return null;
		}
	}

	@ApiMethod(description = "Delete deployment information")
	@RequestMapping(value = "/delete/deployment/{deploymentid}/extractor/{extractorid:.+}", method = RequestMethod.DELETE)
	public DeleteReply removeDeployment(
			@ApiPathParam(description = "The id of the extractor") @PathVariable("extractorid") String extractorId,
			@ApiPathParam(description = "The id of the deployment") @PathVariable("deploymentid") String deploymentId)
					throws Exception {

		ExtractorSpecification saved = this.specRepo.findById(extractorId);
		if (saved != null) {
			int index = -1;
			for (int i = 0; i < saved.getDeployedAt().size(); i += 1) {
				if (saved.getDeployedAt().get(i).contains(deploymentId)) {
					index = i;
				}
			}

			if (index > -1) {
				saved.getDeployedAt().remove(index);
			}
			return new DeleteReply("OK", 1);

		} else {
			return new DeleteReply("ERROR", 0);
		}
	}

	@ApiMethod(description = "Find extractors having a specific name (like query)")
	@RequestMapping(value = "/find/extractors/name/{name:.+}", method = RequestMethod.GET, produces = "application/json")
	public FindReply findByName(
			@ApiPathParam(description = "The name of the extractor") @PathVariable("name") String name)
					throws Exception {
	
		List<ExtractorSpecification> l = this.specRepo.findByNameIgnoreCaseContaining(name);
		FindReply reply = new FindReply();
		reply.setSpecs(l);
		return reply;
	}
	
	@ApiMethod(description = "Find extractors having a specific name (like query)")
	@RequestMapping(value = "/find/extractors", method = RequestMethod.GET, produces = "application/json")
	public FindReply findAll()
					throws Exception {
	
		List<ExtractorSpecification> l = (List<ExtractorSpecification>) this.specRepo.findAll();
		FindReply reply = new FindReply();
		reply.setSpecs(l);
		return reply;
	}

	@ApiMethod(description = "Find extractors containing modes having a specific description (like query)")
	@RequestMapping(value = "/find/extractors/mode/{keyword:.+}", method = RequestMethod.GET, produces = "application/json")
	public FindReply findByModeDescription(
			@ApiPathParam(description = "The term to search for in the mode descriptions") @PathVariable("keyword") String keyword)
					throws Exception {

		List<ExtractorSpecification> l = this.specRepo.findByModeDescription(keyword);
		FindReply reply = new FindReply();
		reply.setSpecs(l);
		return reply;
	}

	@ApiMethod(description = "Find extractors providing specific types (semantic and syntactic types are optional)")
	@RequestMapping(value = "/find/extractors/input", method = RequestMethod.POST, produces = "application/json")
	public FindReply findByInputTypeParams(@RequestBody QueryParameters queryParams) throws Exception {

		List<ExtractorSpecification> results = this.specRepo.findByInputParams(queryParams.getMode(),
				queryParams.getMimeType(), queryParams.getSyntacticType(), queryParams.getSemanticType());
		FindReply reply = new FindReply();
		reply.setSpecs(results);
		return reply;
	}

	@ApiMethod(description = "Find extractors providing specific types (semantic and syntactic types are optional)")
	@RequestMapping(value = "/find/extractors/output", method = RequestMethod.POST, produces = "application/json")
	public FindReply findByOutputTypeParams(@RequestBody QueryParameters queryParams) throws Exception {

		List<ExtractorSpecification> results = this.specRepo.findByOutputParams(queryParams.getMode(),
				queryParams.getMimeType(), queryParams.getSyntacticType(), queryParams.getSemanticType());
		FindReply reply = new FindReply();
		reply.setSpecs(results);
		return reply;
	}

}
