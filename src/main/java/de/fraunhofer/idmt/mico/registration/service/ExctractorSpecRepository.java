package de.fraunhofer.idmt.mico.registration.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import de.fraunhofer.idmt.mico.registration.gen.ExtractorSpecification;

public interface ExctractorSpecRepository extends CrudRepository<ExtractorSpecification, Long> {

	/**
	 * This method will find an ExtractorSpecification instance in the database
	 * by its id. Note that this method is not implemented and its working code
	 * will be automagically generated from its signature by Spring Data JPA.
	 */
	public ExtractorSpecification findById(String id);

	public List<ExtractorSpecification> findByNameIgnoreCaseContaining(String name);

	@Query("SELECT e FROM ExtractorSpecification e JOIN e.mode modes JOIN modes.input inputs JOIN inputs.dataType.mimeTypeItems items "
			+ "WHERE LOWER(modes.id) LIKE LOWER(CONCAT('%',:modeId,'%')) "
			+ "AND (LOWER(inputs.semanticType.name) LIKE LOWER(CONCAT('%',:semanticType,'%')) OR :semanticType IS NULL) "
			+ "AND LOWER(items.item) LIKE LOWER(CONCAT('%',:mimeType,'%')) "
			+ "AND (LOWER(inputs.dataType.syntacticType) LIKE LOWER(CONCAT('%',:syntacticType,'%')) OR :syntacticType IS NULL)")
	public List<ExtractorSpecification> findByInputParams(@Param("modeId") String modeId,
			@Param("mimeType") String mimeType, @Param("syntacticType") String syntacticType,
			@Param("semanticType") String semanticType);

	@Query("SELECT e FROM ExtractorSpecification e JOIN e.mode modes JOIN modes.output outputs JOIN outputs.dataType.mimeTypeItems items "
			+ "WHERE LOWER(modes.id) LIKE LOWER(CONCAT('%',:modeId,'%')) "
			+ "AND (LOWER(outputs.semanticType.name) LIKE LOWER(CONCAT('%',:semanticType,'%')) OR :semanticType IS NULL) "
			+ "AND LOWER(items.item) LIKE LOWER(CONCAT('%',:mimeType,'%')) "
			+ "AND (LOWER(outputs.dataType.syntacticType) LIKE LOWER(CONCAT('%',:syntacticType,'%')) OR :syntacticType IS NULL)")
	public List<ExtractorSpecification> findByOutputParams(@Param("modeId") String modeId,
			@Param("mimeType") String mimeType, @Param("syntacticType") String syntacticType,
			@Param("semanticType") String semanticType);

	@Query("SELECT e FROM ExtractorSpecification e JOIN e.mode modes "
			+ "WHERE LOWER(modes.description) LIKE LOWER(CONCAT('%',:description,'%'))")
	public List<ExtractorSpecification> findByModeDescription(@Param("description") String description);

}
