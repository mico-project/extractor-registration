package de.fraunhofer.idmt.mico.registration.service;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Reads properties from the 'application.properties' file and creates beans.
 * (Annotating a class with the @Configuration indicates that the class can be
 * used by the Spring IoC container as a source of bean definitions.)
 * 
 * The default.properties file is part of the project jar or war file, and
 * consequently available on the class path. However, now we can see that the
 * addition of the ignoreResourcesNotFound attribute allow us to optionally
 * override one or more of the properties depending on the deployment
 * environment. The order of the property file declarations is important, i.e.
 * files declared later will override any previous value(s) if they contain the
 * same key(s).
 * 
 * @author hss
 * 
 */
@Configuration
@PropertySources({ @PropertySource("classpath:application.properties"),
		@PropertySource(value = "file:${catalina.home}/conf/mico/registration-service/application.properties", ignoreResourceNotFound = true) })
public class SpringConfiguration {

	static final Logger log = Logger.getLogger(SpringConfiguration.class);

	@Bean
	public XMLManager xmlManager() {
		XMLManager xmlManager;
		try {
			xmlManager = new XMLManager();
			return xmlManager;
		} catch (Exception e) {
			log.fatal("Bean creation failed: " + e.toString());
			return null;
		}
	}

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper;
	}
}
